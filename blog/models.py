from django.db import models

# Create your models here.
class Post(models.Model):
	title = models.CharField(max_length=200)
	body = models.TextField()

	def __unicode__(self):
		return self.title

	def has_many_comment(self):
		return self.comment_set.count() > 3

class Comment(models.Model):
	post = models.ForeignKey(Post)
	body = models.TextField()

	def __unicode__(self):
		return self.body
