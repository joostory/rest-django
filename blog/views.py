from blog.models import Post, Comment
from django.http import HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
# Create your views here.

def index(request):
	posts = Post.objects.all()
	return render_to_response("blog/posts/index.html", {
		"posts": posts,
	})

def posts_show(request, post_id):
	post = get_object_or_404(Post, pk=post_id)
	return render_to_response("blog/posts/show.html", {
		"post": post,
	})
