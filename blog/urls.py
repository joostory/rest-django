from django.conf.urls import patterns, include, url
from djangorestframework.views import ListOrCreateModelView, InstanceModelView
from blog.resources import PostResource, CommentResource

urlpatterns = patterns('blog.views',
    # Examples:
    # url(r'^$', 'sample.views.home', name='home'),
    # url(r'^sample/', include('sample.foo.urls')),

    url(r'^$', ListOrCreateModelView.as_view(resource=PostResource)),
    url(r'^(?P<pk>\d+)$', InstanceModelView.as_view(resource=PostResource)),
    url(r'^(?P<post>\d+)/comments$', ListOrCreateModelView.as_view(resource=CommentResource)),
    url(r'^(?P<post>\d+)/comments/(?P<pk>\d+)$', InstanceModelView.as_view(resource=CommentResource)),
)
