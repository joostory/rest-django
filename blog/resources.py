from djangorestframework.resources import ModelResource
from djangorestframework.reverse import reverse
from blog.models import Post, Comment

class PostResource(ModelResource):
	model = Post
	fields = ('title', 'body', 'comments')

	#def comments(self, instance):
	#	return reverse('comments', kwargs={'post': instance.id}, request=self.request)

class CommentResource(ModelResource):
	model = Comment
	fields = ('body', 'post')

	#def post(self, instance):
	#	return reverse('post', kwargs={'post': instance.id}, request=self.request)
